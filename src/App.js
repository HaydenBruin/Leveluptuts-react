import React, { Fragment, Component } from 'react';
import logo from './logo.svg';
import './App.css';
import UserProvider from './UserProvider';
import User from './User';

import { Modal, Toggle } from './Utilities/index';

class App extends Component {
    render() {
        return (
            <UserProvider>
                <div className="App">
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo" />
                        <h1 className="App-title">Welcome to React</h1>
                    </header>
                    <p className="App-intro">
                        To get started, edit <code>src/App.js</code> and save to reload.
                    </p>

                    <User />

                    <Toggle>
                        {({on, toggle}) => (
                            <Fragment>
                                <button onClick={toggle}>Login</button>
                                <Modal on={on} toggle={toggle}>
                                    <form>
                                        <h1>Contact Us</h1>

                                        <div className="field">
                                            <label>Your Name</label>
                                            <input type="text" />
                                        </div>

                                        <div className="field">
                                            <label>Your Email</label>
                                            <input type="text" />
                                        </div>

                                        <button>Submit form</button>
                                    </form>
                                </Modal>
                            </Fragment>
                        )}
                    </Toggle>
                    
                </div>
            </UserProvider>
        );
    }
}

export default App;
