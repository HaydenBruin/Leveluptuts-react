import React, { Component } from 'react';
import { Transition } from 'react-spring';

import { Portal, Icon } from './index';

export default class Modal extends Component {
    render() {
        const { children, toggle, on } = this.props;
        return (
            <Portal>
                <Transition from={{ opacity: 0 }} enter={{ opacity: 1 }} leave={{ opacity: 0 }} >
                    {on && (styles =>
                        <div className="modal" style={{ ...styles }}>
                            <div onClick={toggle}><Icon name="close" /></div>
                            <div className="window">{children}</div>
                        </div>  
                    )}
                </Transition>
            </Portal>
        )
    }
}